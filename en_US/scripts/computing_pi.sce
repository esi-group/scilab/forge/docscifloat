// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Computing pi with Archimede's polygons formula
// http://en.wikipedia.org/wiki/Floating_point


// First formula : bad
t = 1/sqrt(3)
for i = 1 : 26
  t = (sqrt(t^2 + 1) - 1)/t;
  p = 6*2^i*t;
  e = abs(p-%pi);
  disp([i t p e])
end

// Second formula : good
t = 1/sqrt(3)
for i = 1 : 26
  t = t/(sqrt(t^2+1)+1);
  p = 6*2^i*t;
  e = abs(p-%pi);
  disp([i t p e])
end

// Reason: cancellation in the term sqrt(t^2 + 1) - 1 when t is small


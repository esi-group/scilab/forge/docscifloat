// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = myhypot_naive(a,b)
  y = sqrt(a^2+b^2)
endfunction
function y = myhypot(a,b)
  if (a==0 & b==0) then
    y = 0;
  else
    if (abs(b)>abs(a)) then
      r = a/b;
      t = abs(b);
    else
      r = b/a;
      t = abs(a);
    end
    y = t * sqrt(1 + r^2);
  end
endfunction
function y = myhypot2(a,b)
  p = max(abs(a),abs(b))
  q = min(abs(a),abs(b))
  while (q<>0.0)
    r = (q/p)^2
    s = r/(4+r)
    p = p + 2*s*p
    q = s * q
  end
  y = p
endfunction


/////////////////////////////////
//
// With naive formula.
//
// A success
myhypot_naive(1,1)
// A failure
myhypot_naive(1.e200,1)
myhypot_naive(1,1.e200)
myhypot_naive(1.e-200,1.e-200)
/////////////////////////////////
//
// With robust formula.
//
myhypot(1,1)
myhypot(1,0)
myhypot(0,1)
myhypot(0,0)
myhypot(1.e200,1)
myhypot(1,1.e200)
/////////////////////////////////
//
// With Moler and Morrison's 
//
myhypot2(1,1)
myhypot2(1,0)
myhypot2(0,1)
myhypot2(0,0)
myhypot2(1,1.e200)
myhypot2(1.e-200,1.e-200)

function y = myhypot2_print(a,b)
  p = max(abs(a),abs(b))
  q = min(abs(a),abs(b))
  i = 0
  while (q<>0.0)
    mprintf("%d %.17e %.17e\n",i,p,q)
    r = (q/p)^2
    s = r/(4+r)
    p = p + 2*s*p
    q = s * q
	i = i + 1
  end
  y = p
endfunction
myhypot2_print(4,3)

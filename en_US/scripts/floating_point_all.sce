// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// One floating point numbers exactly representing a real number
flps = flps_format2system ( 2 , 3 , 3 );
flps_systemprint ( flps );

// Display all
h = flps_systemgui ( flps )

// Display only positive
h = flps_systemgui ( flps , %t , %t )

// Display only positive, but not denormals
h = flps_systemgui ( flps , %f , %t )

// Display only positive in logscale
h = flps_systemgui ( flps , %t , %t , %t )


